class AuthorSessionsController < ApplicationController

   #In author_sessions_controller all the methods need to be accessible to allow login and logout

    #layout "admin" 和 		render :layout => 'admin' 一个意思

    layout 'admin'


	def new
		#render :layout => false 把layout去掉
		if logged_in?
			redirect_to root_path
		end
	end

	def create
		if login(params[:email], params[:password] )
			redirect_back_or_to(articles_path, notice: 'Logged in successfully.')
		else
			flash.now.alert = "Login failed."
			render :new
		end
	end

	def destroy
		logout
		redirect_to(root_path, notice: 'Logged out!')
	end
end
