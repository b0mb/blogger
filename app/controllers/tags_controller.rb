class TagsController < ApplicationController
	before_filter :require_login, only: [:destroy]

	def show
		@tag = Tag.find_by_name(params[:id])
		@tags = Tag.all
		@test = @tag.articles.paginate(:page => 1, :per_page => 4)
	end

	def index
		@tags = Tag.all
	end

	private
	  def not_authenticated
        redirect_to login_path, :alert => "First log in to view this page."
    end
end
