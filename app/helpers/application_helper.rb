module ApplicationHelper
  def markdown(text)
    renderer = HTMLwithPygments.new(hard_wrap: true, filter_html: true)
    options = {
      autolink: true,
      no_intra_emphasis: true,
      fenced_code_blocks: true,
      lax_html_blocks: true,
      strikethrough: true,
      superscript: true
    }
    sanitize Redcarpet::Markdown.new(renderer, options).render(text)
    #没用html_safe 据说不安全 用sanitize
  end

  class HTMLwithPygments < Redcarpet::Render::HTML
    def block_code(code, language)
      language = "text" if language.blank?
      sha = Digest::SHA1.hexdigest(code)
      Rails.cache.fetch ["code", language, sha].join("-") do
        Pygments.highlight(code, :lexer => language)
      end
    end
  end

  def flash_class(level)
    case level
    when :notice then "alert alert-info"
    when :success then "alert alert-success"
    when :error then "alert alert-error"
    when :alert then "alert alert-danger"
    else "alert #{level}"
    end
  end
end
