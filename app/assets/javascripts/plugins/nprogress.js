NProgress.configure({
  speed: 300,
  minimum: 0.03,
  ease: 'ease'
});

$(document).on('page:fetch', function() {
  return NProgress.start();
});

$(document).on('page:restore', function() {
  return NProgress.remove();
});

$(document).on('page:change', function() {
  return NProgress.done();
});