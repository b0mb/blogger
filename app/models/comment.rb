class Comment < ActiveRecord::Base
  belongs_to :commentable, polymorphic: true, counter_cache: true
  validates :body, presence: true, length: { minimum: 6 }
  validates :author_name, presence: true
end
