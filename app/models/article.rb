class Article < ActiveRecord::Base
	extend FriendlyId
	friendly_id :title, use: :slugged

	has_many :taggings
	has_many :tags, through: :taggings
	has_many :comments, as: :commentable
	validates :title, presence: true
	validates :body, presence: true
	validates :tag_list, presence: true

  searchable do
    text :title, :body, :stored => true
  end

	def tag_list
		self.tags.collect do |tag|
			tag.name
		end.join(", ")
	end

	def tag_list=(tags_string)
		tag_names = tags_string.split(",").collect{|s| s.strip.downcase}.uniq
		new_or_found_tags = tag_names.collect{|name| Tag.find_or_create_by(name: name) }
		self.tags = new_or_found_tags
	end

end
